const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Product Name is required!"]
	},
	description: {
		type: String,
		required: [true, "Product description is required!"]
	},
	price: {
		type: Number,
		required: [true, "Product Price is required!"]
	},
	availableInStock: {
		type: Number,
		required: [true, "Available product in stock is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
	default: new Date()
	},
	orders:[{
		userId:{
			type: String,
			required: [true, "Order ID is required!"]
		},
		price: {
			type: Number,
			required: [true, "Product Price is required"]
		},
		quantity: {
			type: Number,
			required: [true, "Product Quantity!"]
		},
		email: {
			type: String,
			required: [true, "User email"]
		}
	}]
})

module.exports = mongoose.model("Products", productSchema);
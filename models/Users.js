const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required!"]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required!"]
	},
	userName:{
		type: String,
		required: [true, "Username is required!"]
	},
	email: {
		type: String,
		required: [true, "Email is required!"]		
	},
	password: {
		type: String,
		required: [true, "Password is required!"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	newOrders:[{
		products:[{
			productName:{
				type: String,
				required: [true, "Product Name is required!"]
			},
			price: {
				type: Number,
				required: [true, "Product Price is required"]
			},
			quantity:{
				type: Number,
				required: [true, "Product Quantity is required!"]
			}
		}],
		totalAmount:{
			type: Number,
			required:[true, "Total Amount is required!"]
		},
		purchasedOn:{
			type: Date,
			default: new Date()
		}
	}],
	total: {
		type: Number,
		required:[false, "Total Order amount"]
	}
})

module.exports = mongoose.model("Users", userSchema);
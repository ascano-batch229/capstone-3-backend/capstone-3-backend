const Products = require ("../models/Products.js");
const User = require ("../models/Users.js");
const auth = require ("../auth.js");

module.exports.addProduct = (data) => {

	if(data.isAdmin == true){

		let newProducts = new Products ({

			productName: data.product.productName,
			description: data.product.description,
			availableInStock: data.product.availableInStock,
			price: data.product.price
		});

		return newProducts.save()
		.then((product,error) => {

			if(error){
				return false;
			}else{
				return true;
			}

		})
	}else{
		return false;
	}
}

module.exports.activeProducts = () => {

	return Products.find({isActive:true})
	.then(result => {
		return result;
	});
}

module.exports.findProduct = (reqParams) => {

	return Products.findById(reqParams.productsId)
	.then(result => {
		return result;
	})

}

module.exports.updateProduct = (data) => {

	if(data.isAdmin == true){

		let updatedProducts = {
			productName: data.product.productName,
			description: data.product.description,
			availableInStock: data.product.availableInStock,
			price: data.product.price
		}


		return Products.findByIdAndUpdate(data.reqParams.productsId, updatedProducts)
		.then((product, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}else{
		return Promise.resolve("Not authorized to access this page");
	}

}

module.exports.archiveProduct = (data) => {

	if(data.isAdmin == true){

		let archivedProduct = {
			isActive: data.product.isActive
		}	

		return Products.findByIdAndUpdate(data.reqParams.productsId, archivedProduct)
		.then((product, error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}else{
		return Promise.resolve("Not authorized to access this page");
	}
}
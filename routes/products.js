const express = require("express");
const router = express.Router();
const productControllers = require("../controllers/productControllers.js");
const auth = require("../auth.js");

router.post("/NewProduct", auth.verify, (req, res) =>{

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin 
	}

	productControllers.addProduct(data)
	.then(fromController => res.send(fromController));
	})

router.get("/active", (req, res) => {
	productControllers.activeProducts()
	.then(fromController => res.send(fromController));
})


router.get("/:productsId", (req, res) => {
	productControllers.findProduct(req.params)
	.then(fromController => res.send(fromController))
})

router.put("/:productsId", auth.verify, (req,res) => {

	const data = {
		product: req.body,
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productControllers.updateProduct(data)
	.then(fromController => res.send(fromController));

})

router.put("/:productsId/archive", auth.verify, (req,res) => {

	const data = {
		product: req.body,
		reqParams: req.params,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productControllers.archiveProduct(data)
	.then(fromController => res.send(fromController));


})

module.exports = router;

 